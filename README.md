# pib-vs_preparation2

## Exercise

### Description

- A server provides a "name search" service over [Java RMI](https://docs.oracle.com/javase/tutorial/rmi/index.html)
- Clients can search for up to `n` names in a predefined list of names

### Request parameters

- Client provides a list of `n` (`n <= 10`) surnames

### Response

- Server returns for each surname given the corresponding first name or an error message if the surname is not in the list

### Requirements

- Must be implemented in Java
- Must use RMI for communication between client and server