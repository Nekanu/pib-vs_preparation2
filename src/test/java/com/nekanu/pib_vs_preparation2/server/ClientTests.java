/*
 * Copyright (c) 2022 Joshua Nestler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.nekanu.pib_vs_preparation2.server;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ClientTests {
    private static Registry registry;
    private static NameFinder nameFinder;

    @BeforeAll
    static void setup() throws RemoteException, NotBoundException {
        // Start the server
        NameFinderServer.main(new String[0]);

        // Find the servers registry and the name finder function registered in it
        registry = LocateRegistry.getRegistry(10564);
        nameFinder = (NameFinder) registry.lookup("NameFinder");

    }

    @Test
    void testMethodRegistration() throws RemoteException {
        assertEquals(1, registry.list().length, "Registry should have 1 entry");

        assertEquals("NameFinder", registry.list()[0], "\"Hello\" method is not registered");
    }

    @Test
    void testSearchWithForeName() throws RemoteException {
        assertEquals("N/A",
                nameFinder.findFirstNames(new String[]{"Isaac"})[0], "Surname \"Isaac\" should not be found");
    }

    @Test
    void testFindFirstNamesSingleSuccess() throws RemoteException {
        assertEquals("Travis",
                nameFinder.findFirstNames(new String[]{"Kerr"})[0],
                "First name of \"Kerr\" should be \"Travis\"");
    }

    @Test
    void testFindFirstNamesMultipleSuccess() throws RemoteException {
        String[] names = nameFinder.findFirstNames(new String[]{"Andrade", "McKinney"});
        assertEquals(2, names.length, "Should find 2 names");
        assertEquals("Bodie", names[0], "First name of \"Andrade\" should be \"Bodie\"");
        assertEquals("Destiny", names[1], "First name of \"McKinney\" should be \"Destiny\"");
    }

    @Test
    void testFindFirstNamesSingleFailure() throws RemoteException {
        assertEquals("N/A",
                nameFinder.findFirstNames(new String[]{"ajsklgfkdjfg"})[0],
                "First name for \"ajsklgfkdjfg\" should not be found");
    }

    @Test
    void testFindFirstNameWithMultipleMatches() throws RemoteException {
        assertEquals("Nadia",
                nameFinder.findFirstNames(new String[]{"Blankenship"})[0],
                "First name for \"Blankenship\" should be \"Nadia\"");
    }
}
