/*
 * Copyright (c) 2022 Joshua Nestler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.nekanu.pib_vs_preparation2.server;

/**
 * This is the interface to be exported to the client over RMI.
 * It provides the functionality to find a name.
 */
public interface NameFinder extends java.rmi.Remote {
    /**
     * Find matching first names to last names in a specified list on the server.
     *
     * @param surnames The surnames to search for.
     * @return The matching first names or "N/A" if no match was found.
     * @throws java.rmi.RemoteException If an error occurs during the remote call.
     */
    String[] findFirstNames(String[] surnames) throws java.rmi.RemoteException;
}
