/*
 * Copyright (c) 2022 Joshua Nestler
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 * and associated documentation files (the “Software”), to deal in the Software without restriction,
 * including without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software,
 * and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 * DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
 * OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

package com.nekanu.pib_vs_preparation2.server;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;


import java.io.Reader;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hello world.
 *
 * @author Joshua Nestler
 */
public class NameFinderServer implements NameFinder {
    /**
     * The logger for this class.
     */
    private static final Logger LOGGER = LoggerFactory.getLogger(NameFinderServer.class);

    /**
     * The port to use for the RMI registry.
     */
    private static final int RMI_REGISTRY_PORT = 10564;

    /**
     * Explicitly set the delimiter to use for CSV parsing.
     */
    private static final CSVFormat CSV_FORMAT = CSVFormat.Builder.create().setDelimiter(",").build();

    /**
     * The amount of values per row in the CSV file.
     */
    private static final int CSV_ROW_SIZE = 2;

    /**
     * The column index of the first name in the CSV file.
     */
    private static final int CSV_COLUMN_FIRST_NAME = 0;

    /**
     * The column index of the first name in the CSV file.
     */
    private static final int CSV_COLUMN_SURNAME = 1;

    /**
     * The path to the CSV file.
     */
    private static final String NAMES_CSV_FILE_PATH = "data/names.csv";

    /**
     * A search optimized map of names.
     */
    private final Map<String, List<String>> names = new HashMap<>();

    /**
     * This is the constructor for the NameFinderServer.
     */
    public NameFinderServer() {
        LOGGER.info("NameFinderServer started");

        // Load the names from the file
        try (Reader reader = new java.io.FileReader(NAMES_CSV_FILE_PATH, java.nio.charset.StandardCharsets.UTF_8)) {
            final CSVParser csvParser = new CSVParser(reader, CSV_FORMAT);

            for (final CSVRecord csvRecord : csvParser) {
                // Disregard records that do not have the correct number of columns
                if (csvRecord.size() != CSV_ROW_SIZE) {
                    LOGGER.warn("Invalid CSV record: {}", csvRecord);
                    continue;
                }

                // Extract the first name and surname
                final String firstName = csvRecord.get(CSV_COLUMN_FIRST_NAME);
                final String surname = csvRecord.get(CSV_COLUMN_SURNAME);

                // Add the first name to the list of names for the surname
                final List<String> firstNames = names.get(surname);
                if (firstNames != null) {
                    firstNames.add(firstName);
                } else {
                    names.put(surname, new java.util.ArrayList<>(List.of(firstName)));
                }
            }
        } catch (Exception e) {
            LOGGER.error("Error loading names from file", e);
        }
    }

    @Override
    public String[] findFirstNames(String[] surnames) throws RemoteException {
        String[] result = new String[surnames.length];

        for (int i = 0; i < surnames.length; i++) {
            final List<String> firstnames = names.get(surnames[i]);
            if (firstnames != null) {
                // Return only the first forename found for  each surname
                result[i] = firstnames.get(0);
            } else {
                // If surname is not found, return "N/A"
                result[i] = "N/A";
            }
        }

        return result;
    }

    /**
     * Main method.
     *
     * @param args Command line arguments
     */
    public static void main(String[] args) {
        try {
            // Will also host the registry on port 10564 since there will not be more than one server
            final Registry registry = LocateRegistry.createRegistry(RMI_REGISTRY_PORT);

            // Create the server and export it to the registry
            final NameFinder nameFinderServer =
                    (NameFinder) UnicastRemoteObject.exportObject(new NameFinderServer(), 0);

            // Bind the server to the registry
            registry.rebind("NameFinder", nameFinderServer);

            LOGGER.info("Server ready");
        } catch (RemoteException e) {
            LOGGER.error("Error in/with RMI", e);
        }
    }
}
